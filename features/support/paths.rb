module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in web_steps.rb
  #
  def path_to(page_name)
    case page_name
    
    when /the home\s?page/
      '/'
    when /login/
      '/login'
    
    when /the new channel page$/
      new_channel_path
      
    when /the channel page for "(.*)"$/
      channel_path(Channel.find_by_name($1))
    
    when /the fetch message page$/
      fetch_url
    
    when /the fetch message page with start ([0-9]*)$/
      fetch_url(:params => {:start => $1})
      
    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
        "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(NavigationHelpers)
