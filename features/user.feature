Feature: Users

  Scenario: Logged in users should see their username and a logout link
    Given I am logged in as "cucumber" with password "monkey"
    Given I am on the home page
    Then I should see "cucumber" within "#user-info"
    And I should see "Logout" within "#user-info"
    