Given /^I am logged in as "(.*)" with password "(.*)"$/ do |login, pass|
  Factory(:user, :login => login, :password => pass, :timezone => "Central Time (US & Canada)")
  visit path_to("login")
  fill_in("login", :with => login)
  fill_in("password", :with => pass)
  click_button("Log in")
end

Given /^I have a channel named "(.*)"$/ do |name|
  Factory(:channel, :name => name)
end

Given /^I have a private channel named "(.*)"$/ do |name|
  Factory(:private_channel, :name => name)
end


Given /^I have ([0-9]*) messages in "([^\"]*)"$/ do |count, channel|
  chan_id = Channel.find_by_name(channel).id
  count.to_i.times {|n| Message.new(:channel_id => chan_id, :message => "Message number #{n+1}", :name => "User#{n+1}").save!}
end

Then /^I should see messages ([0-9]*) to ([0-9]*) on page ([0-9]*) in "(.*)"$/ do |from, to, page, container|
  if from.to_i > to.to_i then
    lower = to.to_i
    upper = from.to_i
  else
    lower = from.to_i
    upper = to.to_i
  end
  lower.upto(upper) do |num|
    Then %{I should see /number #{num}$/ within "#{container}"}
  end
end

Then /^I should not see messages ([0-9]*) to ([0-9]*) on page ([0-9]*) in "(.*)"$/ do |from, to, page, container|
  if from.to_i > to.to_i then
    lower = to.to_i
    upper = from.to_i
  else
    lower = from.to_i
    upper = to.to_i
  end
  lower.upto(upper) do |num|
    Then %{I should not see /number #{num}$/ within "#{container}"}
  end
end

Then /^my session's active channel should be "([^\"]*)"$/ do |channel|
  channel_id = Channel.find_by_name(channel).id
  session[:active_channel].should == channel_id
end

Then /^my session's active channel should not be "([^\"]*)"$/ do |channel|
  bad_channel_id = Channel.find_by_name(channel).id
  session[:active_channel].should_not == bad_channel_id
end

Then /^I should get (.*) message[s]?$/ do |num|
  response_text = JSON.parse(response.body)
  response_text["messages"].length.should == num.to_i
end

Then /^([0-9]*) messages get added to "([^\"]*)"$/ do |count, channel|
  chan_id = Channel.find_by_name(channel).id
  count.to_i.times {|n| Message.new(:channel_id => chan_id, :message => "New message #{n}", :name => "New").save!}
end
