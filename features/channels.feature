Feature: Channels
  In order to chat
  Users must be able to manage and view channels
  
  Scenario: Create a channel
    Given I am logged in as "cucumber" with password "monkey"
    Given I am on the home page
    When I follow "New Channel"
    Then I should be on the new channel page
    When I fill in "Name" with "Test Channel"
    And I press "Create"
    Then I should be on the channel page for "Test Channel"
    When I go to the home page
    Then I should see "Test Channel" within "#channel-list"
  
  Scenario: Users should not see private channels in channel list
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I have a channel named "Channel 2"
    Given I have a private channel named "private_channel"
    Given I am on the home page
    Then I should see "Channel 1" within "#channel-list"
    And I should see "Channel 2" within "#channel-list"
    And I should not see "private_channel" within "#channel-list"
    
  Scenario: Requesting a channel should set that channel as the active channel
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I have a channel named "Channel 2"
    When I go to the channel page for "Channel 1"
    Then my session's active channel should be "Channel 1"
    And my session's active channel should not be "Channel 2"
    
  Scenario: Channels should have a list of active users
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    When I go to the channel page for "Channel 1"
    Then I should see "cucumber" within "#userlist"