Feature: Messages
  In order to chat
  Users must be able to send and recieve messages
  
  Scenario: Messages sent should show up in the channel
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I am on the home page
    When I follow "Channel 1"
    Then I should be on the channel page for "Channel 1"
    When I fill in "message_message" with "Message for channel 1"
    And I press "Send"
    Then I should see "Message for channel 1" within "#messages"

  Scenario: Loading messages should give 30 messages per page
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I have 60 messages in "Channel 1"
    When I go to the channel page for "Channel 1"
    Then I should see messages 31 to 60 on page 1 in "#messages"
    And I should not see messages 1 to 30 on page 1 in "#messages"
    
  Scenario: Loading messages with a start parameter should give only messages after the start parameter
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I have 20 messages in "Channel 1"
    Given I am on the channel page for "Channel 1"
    When I go to the fetch message page with start 15
    Then I should get 5 messages
    When I go to the fetch message page with start 19
    Then I should get 1 message
  
  Scenario: Fetching messages should only get the newest messages
    Given I am logged in as "cucumber" with password "monkey"
    Given I have a channel named "Channel 1"
    Given I have 20 messages in "Channel 1"
    Given I am on the channel page for "Channel 1"
    When I go to the fetch message page
      Then I should get 0 messages
    Then 3 messages get added to "Channel 1"
    When I go to the fetch message page
      Then I should get 3 messages
    When I go to the fetch message page
      Then I should get 0 messages
    