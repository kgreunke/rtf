class ChannelSession < ActiveRecord::Base
  belongs_to :user
  belongs_to :channel
  
  validates_presence_of :user_id, :on => :create, :message => "can't be blank"
  validates_presence_of :channel_id, :on => :create, :message => "can't be blank"
  
  def self.create_or_update(user, channel)
    session = find(:first, :conditions => { :user_id => user, :channel_id => channel})
    if session
      session.touch
    else
      create!(:user_id => user, :channel_id => channel)
    end
  end
end
