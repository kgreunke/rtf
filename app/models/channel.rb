class Channel < ActiveRecord::Base
  has_many :messages
  has_many :channel_sessions
  
  def self.find_public_channels
    Channel.all(:conditions => {:private => false})
  end
end
