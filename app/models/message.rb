class Message < ActiveRecord::Base
  belongs_to :channel

  attr_accessible :channel_id, :name, :message
  
  validates_presence_of :message
  def self.per_page
    30
  end
  
  def display_created_at(zone)
    created_at.in_time_zone(zone).strftime("%Y-%m-%d &nbsp; %H:%M")
  end
end
