class window.Channel extends Backbone.Model
  defaults:
    active: false
    class: ''
    unread: 0
    pm: false

  initialize: ->
    # Common options.  Model specific setup goes in setup()
    # Set up the messages collection for this channel
    @messages = new Messages()
    # Set up the view
    @$inputBox = $('<input type="text" placeholder="Type message here"></input>')
    @messageList = new MessageListView
      collection: @messages
      inputBox: @$inputBox
    @setup()
    if @collection
      @collection.mediator.on "channel:status:update", @updateStatus
      @collection.mediator.on "focus:page", @focus

  setup: ->
    @messages.url = @url() + '/messages'
    @messages.fetch()
    # Subscribe to the channel for push updates
    @sub = @collection.mediator.transport().subscribe "/channel/public/#{@id}", @getMessage
    @sub.callback (=> Logger.log "Channel::setup", "Subscription to channel #{@get "name"} complete")
    @$inputBox.keydown @sendMessage

  updateStatus: (id) =>
    if id == @cid
      Logger.log "Channel::updateStatus", "#{@attributes.name}: active"
      @set {active: true, class: 'current'}
      @focus()
    else
      Logger.log "Channel::updateStatus", "#{@attributes.name}: inactive"
      @set {active: false, class: ''}

  active: ->
    @get 'active'

  getMessage: (msg) =>
    @messages.add msg
    # If the browser window or channel are not focused, add to unread count
    unless @attributes.active and @collection.mediator.localStatus()
      Logger.log "Channel::getMessage", "Channel status: #{@attributes.active}, LocalStatus: #{@collection.mediator.localStatus()}"
      @set
        unread: @attributes.unread + 1
      @updateUnreads()

  sendMessage: (event) =>
    if event.keyCode == 13
      message =
        message:
          name: @collection.mediator.currentUserName()
          message: @$inputBox.val()
          channel_id: @attributes.id
        authenticity_token: @collection.token
      $.ajax
        url: '/messages.json'
        type: 'POST'
        contentType: 'application/json'
        processData: false
        dataType: 'json'
        data: JSON.stringify(message)
        success: @clearInput

  clearInput: =>
    @$inputBox.val ""

  renderMessages: =>
    Logger.log "Channel::renderMessages", @attributes.name
    $('#input').children().detach()
    $('#input').append @$inputBox
    $('#messages').children().detach()
    $('#messages').append @messageList.el
    $(window).trigger 'resize'
    $('#messages').prop 'scrollTop', 9999
    @collection.mediator.trigger "channel:status:update", @cid

  focus: =>
    Logger.log "Channel::focus", "#{@attributes.name} - Active: #{@active()}"
    if @active()
      if @attributes.unread > 0
        @set {unread: 0}
        @updateUnreads()
      @$inputBox.focus()
      
  updateUnreads: =>
    Logger.log "Channel::updateUnreads", JSON.stringify([@id, @attributes.pm, @attributes.unread])
    @collection.mediator.trigger "channel:unreads", [@id, @attributes.pm, @attributes.unread]
