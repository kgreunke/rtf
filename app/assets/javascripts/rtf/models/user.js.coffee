class window.User extends Channel
  defaults:
    active: false
    class: ''
    unread: 0
    avatar_url: "/assets/avatars/default.png"
    pm: true

  setup: ->
    if @collection
      @collection.mediator.on 'user:status', @setStatus
      @collection.mediator.on 'user:message', @getMessage
      @collection.mediator.on 'user:refresh', @refresh
      @messages.on "add", @persist
      $(@$inputBox).keydown @sendMessage
      @id = [@collection.mediator.currentUserName(), @attributes.name].sort().join ""
      @channel = "/channel/priv/#{@id}"
      @sub = @collection.mediator.transport().subscribe @channel, @getMessage
      @hydrate()
    @set {avatar_url: "https://robohash.org/#{@get('name').toLowerCase()}?size=48x48&bgset=bg2"}

  valid: =>
    @attributes.name?

  setStatus: (msg) =>
    if msg.name == @attributes.name then @set {status: msg.status}

  sendMessage: (event) =>
    if event.keyCode == 13
      message =
        name: @collection.mediator.currentUserName()
        to: @attributes.name
        created_at: Date.now()
        message: @$inputBox.val()
      @collection.transport.publish @channel, message
      @$inputBox.val ""

  persist: () =>
    if window.localStorage?
      while @messages.length > 80
        @messages.shift()
      window.localStorage.setItem(@id, JSON.stringify(@messages))

  hydrate: () =>
    if window.localStorage?
      messages = window.localStorage.getItem(@id)
      @messages.reset(JSON.parse(messages))

  refresh: =>
    @set {status: "offline"}
