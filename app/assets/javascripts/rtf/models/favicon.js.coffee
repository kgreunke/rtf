class window.Favicon

  constructor: (options) ->
    @mediator = options.mediator
    @alerting = false
    @alertInterval
    
    @el = $('<link rel="icon" type="image/png"></link>')

    @mediator.on "channel:unreads", @startAlert
    @mediator.on "focus:page", @stopAlert

    @normalIcon = @drawNormal()
    @alertIcon = @drawAlert()

    @setNormal()
    $('head').append @el

  startAlert: =>
    if not @alerting and not @mediator.localStatus()
      @flip = true
      @alertInterval = setInterval @alert, 1000
      @alerting = true

  stopAlert: =>
    if @alerting
      clearInterval @alertInterval
      @alerting = false
      @setNormal()

  alert: =>
    if @flip
      @setAlert()
    else
      @setNormal()
    @flip = not @flip

  setNormal: =>
      @el.attr "href", @normalIcon.toDataURL "image/png"

  drawNormal: ->
    canvas = document.createElement "canvas"
    canvas.width = canvas.height = 16
    ctx = canvas.getContext "2d"
    pic = Math.PI * 2

    ctx.fillStyle= "666"

    ctx.beginPath()
    ctx.arc(6,6,6,0,pic,true)
    ctx.arc(10,6,6,0,pic,true)
    ctx.moveTo(4,6)
    ctx.lineTo(2,16)
    ctx.lineTo(14,9)
    ctx.fill()
    ctx.fillRect(6,0,4,12)

    canvas
  
  setAlert: =>
    @el.attr "href", @alertIcon.toDataURL "image/png"

  drawAlert: ->
    canvas = document.createElement "canvas"
    canvas.width = canvas.height = 16
    ctx = canvas.getContext "2d"
    pic = Math.PI * 2

    ctx.fillStyle= "AE432E"

    ctx.beginPath()
    ctx.arc(6,6,6,0,pic,true)
    ctx.arc(10,6,6,0,pic,true)
    ctx.moveTo(4,6)
    ctx.lineTo(2,16)
    ctx.lineTo(14,9)
    ctx.fill()
    ctx.fillRect(6,0,4,12)
    ctx.clearRect(7,2,2,4)
    ctx.clearRect(7,8,2,2)
  
    canvas
