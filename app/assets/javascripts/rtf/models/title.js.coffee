class window.Title extends Backbone.Model
  defaults:
    cm: 0
    pm: 0
    spin: "|"

  initialize: (options={}) ->
    @mediator = options.mediator
    @msgs = {}
    @pms = {}
    @spinChars = ["|", "/", "-", "\\"]
    @spinCount = 0
    @spinning = false

    # Events
    @.bind 'change:pm change:cm', @unreads
    @mediator.on "channel:unreads", @update
    @mediator.on "focus:page", @stopSpin

    # View
    @view = new TitleView {model: @}
    @view.render()


  update: (params) =>
    [channel, pm, count] = params
    if pm
      @pms[channel] = count
      @set {pm: @sumIt(@pms)}
    else
      @msgs[channel] = count
      @set {cm: @sumIt(@msgs)}

  unreads: ->
    if @attributes["pm"] + @attributes["cm"] > 0
      @set {total: true}
      @startSpin()
    else
      @set {total: false}

  addPM: (num) =>
    @set {pm: @attributes["pm"] + num}

  addCM: (num) =>
    @set {cm: @attributes["cm"] + num}

  sumIt: (vals) =>
    valArray = (value for key,value of vals)
    valArray.reduce (x, y) -> x + y

  startSpin: =>
    if not @spinning and not @mediator.localStatus()
      @spinCount = 0
      @spinning = true
      @spinInterval = setInterval @spin, 1000

  stopSpin: =>
    if @spinning
      clearInterval @spinInterval
      @set {spin: "|"}
      @spinning = false

  spin: =>
    @set {spin: @spinChars[@spinCount]}
    @spinCount = if @spinCount == 3 then 0 else @spinCount+1

