class window.Users extends Backbone.Collection
  model: User

  initialize: (models, options) ->
    @mediator = options.mediator

  addUnique: (user) ->
    userRec = @detect( (u) -> user.name == u.get('name') )
    if userRec
      userRec.setStatus(user)
    else
      @add user

