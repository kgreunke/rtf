class window.Channels extends Backbone.Collection
  model: Channel
  url: '/channels'

  initialize: (models, options) ->
    @mediator = options.mediator
