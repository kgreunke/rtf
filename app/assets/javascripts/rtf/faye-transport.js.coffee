class FayeTransport

  init: () ->
    unless Faye
      @faye = new Faye.Client window.location.href + 'framework/push', {timeout: 120}
      @faye.disable 'websocket'
    else
      console?.log "ERROR :: Faye transport not defined"

  subscribeUser: (userName, fn) ->
    @faye.subscribe "/channel/priv/#{userName}", fn

  subscribeChannel: (channelName, fn) ->
    @faye.subscribe "/channel/public/#{channelName}", fn

  subscribeControl: (fn) ->
    @faye.subscribe "/control", fn

  
