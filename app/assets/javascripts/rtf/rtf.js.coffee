class window.RTF
  constructor: (options={})->
    @formToken = options.token
    @mediator = new Mediator @
    @currentUser = new User
      name: options.name
      mediator: @mediator
    #Faye.Logging.logLevel = 'debug'
    #Faye.logger = (msg) -> Logger.log "FAYE", msg
    #@faye = new Faye.Client 'http://localhost:9293/framework/push', {timeout: 60}
    @faye = new Faye.Client window.location.href + 'framework/push', {timeout: 120}
    @faye.disable 'eventsource'
    @users = new Users [],
      mediator: @mediator
    @users.transport = @faye
    @users.currentUser = @currentUser.get 'name'
    @channels = new Channels [],
      mediator: @mediator
    @channels.transport = @faye
    @channels.currentUser = @currentUser.get 'name'
    @channels.token = @formToken
    @title = new Title {mediator: @mediator}
    @favicon = new Favicon {mediator: @mediator}
    @appView = new AppView
      currentUserView:  new CurrentUserView {model: @currentUser}
      userListView:     new UserListView {collection: @users }
      channelListView:  new ChannelListView {collection: @channels}

  init: ->
    @localStatus = "active"
    @currentUser.set {status: "active"}
    @appView.render()
    @resizeMessages()
    @events()
    @subs()
    @channels.fetch()

  events: ->
    $(window).on 'blur', false, _.debounce @focusEvent, 500
    $(window).on 'focus', true, _.debounce @focusEvent, 500
    $(window).resize @resizeMessages

  subs: ->
    @controlSub = @faye.subscribe "/control", @control
    @controlSub.callback @refreshUsers

  send: (action, payload={}, channel="/control") =>
    message =
      action: action
      payload: payload
    Logger.log "Core::send", JSON.stringify message
    @faye.publish channel, message
    
  control: (message) =>
    Logger.log "Core::control", JSON.stringify message
    switch message.action
      when "status" then @controlStatus message.payload
      when "update" then @controlUpdate message.payload
      when "refresh" then @controlRefresh()

  controlStatus: (status={}) =>
    Logger.log "Core::controlStatus", JSON.stringify status
    @mediator.trigger "user:status", status

  controlUpdate: (user) =>
    unless user.name == @currentUser.get 'name'
      @users.addUnique user

  controlRefresh: =>
    Logger.log "Core::controlRefresh", ""
    @mediator.trigger "user:refresh"
    clearTimeout @refreshTimeout
    @refreshTimeout = setTimeout @refreshUsers, (Math.round Math.random()*60000+330000, 0)
    @send "update", {name: @currentUser.get('name'), status: @currentUser.get('status')}

  refreshUsers: =>
    @send "refresh"

  focusEvent: (event) =>
    Logger.log "Core::focusEvent", JSON.stringify event.data
    if event.data
      @setCurrentStatus "active"
      @mediator.trigger "focus:page", true
    else
      @setCurrentStatus "away"
      @mediator.trigger "focus:page", false

  setCurrentStatus: (status) ->
    clearTimeout @statusTimeout
    @localStatus = status
    if @localStatus != @currentUser.get('status')
      @statusTimeout = setTimeout (=>
        @currentUser.set {status}
        @sendCurrentStatus()
      ), 5000
      Logger.log "Core::setCurrentStatus", status

  sendCurrentStatus: =>
    currentstatus =
      name: @currentUser.get 'name'
      status: @currentUser.get 'status'
    @send "status", currentstatus

  getCurrentStatus: =>
    @currentUser.get 'status'

  resizeMessages: ->
    $el = $('#messages')
    heightOffset = $el.outerHeight() - $el.height()
    $el.css 'height', window.innerHeight - $('#input').outerHeight() - heightOffset

  transport: ->
    @faye

  currentUserName: ->
    @currentUser.get 'name'

  formToken: ->
    @formToken
