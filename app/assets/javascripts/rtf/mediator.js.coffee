class window.Mediator

  constructor: (core) ->
    _.extend @, Backbone.Events
    @core = core

  transport: ->
    @core.transport()

  currentUserName: ->
    @core.currentUserName()

  formToken: ->
    @core.formToken()

  localStatus: ->
    #Logger.log "Mediator::localStatus", @core.localStatus
    if @core.localStatus == "away"
      false
    else if @core.localStatus == "active"
      true
    else
      false
