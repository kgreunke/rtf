class window.MessageView extends Backbone.View
  tagName: 'div'
  className: 'message'

  initialize: ->
    @template = Handlebars.compile $("#message-template").html()

  render: =>
    message = @model.toJSON()
    message.created_at = @formatDate message.created_at
    message.message = @linkify message.message
    renderedContent = @template message
    ($ @el).html renderedContent
    @

  formatDate: (date) ->
    dateObj = new Date(date)
    if dateObj.getMinutes() < 10 then minutes = "0#{dateObj.getMinutes()}" else minutes = dateObj.getMinutes()
    if dateObj.getHours() < 0 or dateObj.getHours() > 12 then hours = Math.abs dateObj.getHours() - 12 else hours = dateObj.getHours()
    "#{hours}:#{minutes}"

  linkify: (message) ->
    message.replace /(http[s]?:\/\/[^\s]*)/g, (url) ->
      shortname = url
      shortname = url.substring(0,79) + "..." if url.length > 80
      "<a href='#{url}' target='_blank'>#{shortname}</a>"

