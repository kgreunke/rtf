class window.MessageListView extends Backbone.View
  tagName: 'div'

  initialize: ->
    @collection.bind 'reset', @render
    @collection.bind 'add', @addOne

  events:
    "click": "focus"

  render: =>
    @collection.each (message) =>
      @addOne message
    @

  addOne: (message) =>
    view = new MessageView
      model: message
    $(@el).append view.render().el
    @scroll()

  focus: =>
    @options.inputBox.focus()

  scroll: ->
    $(@el).parent().animate({scrollTop: $(@el).prop('scrollHeight')}, 500)

