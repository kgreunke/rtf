class window.ChannelListView extends Backbone.View
  tagName: 'nav'
  className: 'main-nav'
  viewClass: ChannelView
  title: "Channels"

  initialize: ->
    @template = Handlebars.compile $("#user-list-template").html()
    @collection.bind 'add', @addOne
    @collection.bind 'reset', @render

  render: =>
    $(@el).empty()
    $(@el).html @template
      title: @title
    @collection.each (model) =>
      @addOne model
    @

  addOne: (model) =>
    view = new @viewClass({model: model})
    @$('ul').append view.render().el

