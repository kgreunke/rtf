class window.ChannelView extends Backbone.View
  tagName: 'li'

  events:
    "click a": "getMessages"

  initialize: ->
    @template = Handlebars.compile $("#user-template").html()
    @model.bind 'change', @render

  render: =>
    Logger.log "ChannelView::render", @model.get 'name'
    $(@el).attr('class', @model.get 'class').html @template @model.toJSON()
    @

  getMessages: (event) ->
    Logger.log "ChannelView::getMessages", "#{@model.get 'name'} clicked"
    if event.preventDefault
      event.preventDefault()
    else
      event.returnValue = false
    if @model.active() then @model.focus() else @model.renderMessages()
