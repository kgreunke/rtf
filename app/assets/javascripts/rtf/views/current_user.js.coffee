class window.CurrentUserView extends Backbone.View
  tagName: 'section'
  id: 'user-info'

  initialize: ->
    @template = Handlebars.compile $("#current-user-template").html()

  render: =>
    $(@el).html @template @model.toJSON()
    @

