class window.AppView extends Backbone.View
  el: "#pagewrapper"

  initialize: ->
    @template = Handlebars.compile $("#app-template").html()
    @currentUserView = @options.currentUserView
    @userListView = @options.userListView
    @channelListView = @options.channelListView


  render: =>
    $(@el).empty().append @template {}
    $("#usercolumn").append @currentUserView.render().el
    $("#usercolumn").append @channelListView.render().el
    $("#usercolumn").append @userListView.render().el
    @

