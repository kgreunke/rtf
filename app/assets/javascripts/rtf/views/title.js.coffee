class window.TitleView extends Backbone.View

  initialize: ->
    @template = Handlebars.compile "{{#if total}}{{cm}}{{spin}}{{pm}} - {{/if}}rtf"
    @model.bind 'change', @render

  render: =>
    document.title = @template @model.toJSON()
    #$(@el).empty().append @template @model.toJSON()
    @

