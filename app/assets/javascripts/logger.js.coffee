EasyTimeout = (delay, fn) -> setTimeout fn, delay

class Logger

  constructor: ->
    @con = console || (-> )

  log: (location, message) ->
    return unless location or message

    @con.log @timestamp() + (@pad location, 30) + message
    @seperator()

  seperator: =>
    clearTimeout @sepTimeout
    @sepTimeout = EasyTimeout 1000, =>
      @con.log "--------------------------------------------------------------------------------"

  timestamp: ->
    date = new Date()
    hours = @zero date.getHours()
    mins = @zero date.getMinutes()
    secs = @zero date.getSeconds()

    output = "[#{hours}:#{mins}:#{secs}] : "
    output

  zero: (time) ->
    if time < 10 then "0#{time}" else time

  pad: (loc, len) ->
    padded = loc
    while padded.length < len
      padded += " "
    padded + " -> "

window.Logger = new Logger()
