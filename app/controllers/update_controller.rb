class UpdateController < ApplicationController
  before_filter :check_xhr  
  def index
    channel_id = session[:active_channel]
    start_id = params[:start] || session[:last_message]|| 0
    @messages = Message.find_all_by_channel_id(channel_id, :conditions => "id > #{start_id}")
    unless @messages.empty?
      @messages_string = render_to_string :partial => 'channels/message', :collection => @messages
      @messages_count = @messages.length
      session[:last_message] = @messages.last.id
    else
      @messages_count = 0
    end
    # message_hash = "{\"messages\":#{@messages.to_json(:only => [:name, :message, :created_at])}}"
    # render :text => message_hash
    respond_to do |format|
      format.json
    end
  end
end
