class ApplicationController < ActionController::Base
  before_filter :require_login
 
  helper :all
 
  protect_from_forgery

  include AuthenticatedSystem

  #filter_parameter_logging :password
 
  def check_xhr
    unless request.xhr?
      redirect_to "/"
    end  
  end

  def helpers
    ActionController::Base.helpers
  end

  private

  def require_login
    unless current_user
      redirect_to login_url
    end
  end

  TIP_URL = "http://www.wowarmory.com/item-tooltip.xml?i="

  def data_cache(key, length = 1.hour)
    unless output = MMCACHE.get(key)
      output = yield
      MMCACHE.set(key, output, length)
    end
    return output
  end

  def build_tooltip(id)
    begin
      body = HTTParty.get(TIP_URL+id).body.map do |line|
        line = "" if line.include? "DOCTYPE"
        line.gsub!("/shared/global/tooltip/images/icons", "/images/tooltip")
        line
      end

      @item_id = id
      @body_string = body.to_s.chomp
      @css_class, @name = @body_string.match(/class="(.*?myItemName.*?)">.*?>(.*?)</)[1..2]
      @full_tip = render_to_string :partial => 'tooltip/tooltip', :layout => false
      data_cache(id, 7.days){{:body => @full_tip, :name => @name, :css => @css_class}}
    rescue
      @tip_data = {:error => "Armory seems to be down"}
    end
  end
end
