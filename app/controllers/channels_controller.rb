class ChannelsController < ApplicationController
  before_filter :check_xhr, :except => [:index]
  
  def index
    @channels = Channel.all
    render :json => @channels, :except => [:created_at, :private, :updated_at]
  end

  def new
    @channel = Channel.new
  end

  def create
    @channel = Channel.new(params[:channel])
    if @channel.save
      flash[:notice] = "Channel created successfully"
      redirect_to channel_path(@channel)
    else
      flash[:error] = "Channel creation failed"
      render :action => 'new'
    end      
  end
  
  def show
    page = params[:page] || 1
    @channel = Channel.find(params[:id])
    @messages = Message.paginate_by_channel_id @channel.id, :page => page, :order => "id DESC"
    session[:active_channel] = @channel.id
    ChannelSession.create_or_update(current_user.id, @channel.id)
    @users = ChannelSession.find_all_by_channel_id(@channel.id, :include => :user, :conditions => ["updated_at > ?", Time.now.utc - 30.minutes])
    if @messages.empty?
      session[:last_message] = 0
    else
      session[:last_message] = @messages[0].id
    end
  end

  private

  def auth_check
    if current_user
      "RTF.login('#{current_user.login}');RTF.fayeClient.subscribe(RTF.MESSAGE_CHANNEL+'/#{current_user.login}', RTF.pmUpdate);"
    else
      "RTF.loginWindow();"
    end
  end

end
