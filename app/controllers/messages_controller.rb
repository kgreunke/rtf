class MessagesController < ApplicationController
  before_filter :check_xhr
  
  def index
    if params[:channel_id]
      @messages = Message.find_all_by_channel_id(params[:channel_id], :limit => 80, :order => "id DESC").reverse
      @output = @messages.map do |m|
        {
          :name => m.name,
          :message => helpers.sanitize(m.message, :tags => %w(b u s i sub sup ul li ol)),
          :created_at => m.created_at
        }
      end
      render :json => @output
    else
      render :text => "Channel missing"
    end
  end

  def new
  end

  def create
    @message = Message.new(params[:message])
    if not @message.message.empty? and @message.save
      Push.message("/channel/public/#{@message.channel_id}", :name => @message.name, :message => helpers.sanitize(@message.message, :tags => %w(b u s i sup sub ul li ol)), :created_at => @message.created_at)
    else
      flash[:error] = "Error saving the message"
    end
    render :status => 200, :layout => false, :text => {:status => 'ok', :flash => flash}.to_json
  end
  
  def show
    @messages = Message.find(:all)
  end
  
  def fetch
    limit = params[:limit] || 80
    @messages = Message.find_all_by_channel_id(1, :limit => limit, :order => "id DESC")
    @messages.reverse!
    @output = {:messages => 
                @messages.map{|message|
                  {
                   :name => message.name,
                   :message => helpers.sanitize(message.message, :tags => %w(b u s i sub sup ul li ol)),
                   :created_at => message.created_at
                  }
                }
              }
    render :json => @output 
  end

end
