class UsersController < ApplicationController
  # Be sure to include AuthenticationSystem in Application Controller instead
  include AuthenticatedSystem
  
  before_filter :check_xhr, :only => [:edit, :update]

  # render new.rhtml
  def new
    @user = User.new
  end
 
  def create
    logout_keeping_session!
    @user = User.new(params[:user])
    success = @user && @user.save
    if success && @user.errors.empty?
            # Protects against session fixation attacks, causes request forgery
      # protection if visitor resubmits an earlier form using back
      # button. Uncomment if you understand the tradeoffs.
      # reset session
      self.current_user = @user # !! now logged in
      redirect_back_or_default('/')
      flash[:notice] = "Thanks for signing up!  We're sending you an email with your activation code."
    else
      flash[:error]  = "We couldn't set up that account, sorry.  Please try again, or contact an admin (link is above)."
      render :action => 'new'
    end
  end
  
  def edit
    @user = User.find(current_user.id)
  end
  
  def update
    @user = User.find(current_user.id)
    respond_to do |format|
      if @user.update_attributes(params[:user])
        session[:timezone] = @user.timezone
        flash[:notice] = "Settings updated successfully"
        format.html {redirect_to '/'}
        format.xml  { head :ok }
      else
        format.html {render :action => 'edit'}
        format.xml  {render :xml => @user.errors, :status => :unprocessable_entity}
      end
    end
  end
end
