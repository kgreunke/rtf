module ChannelsHelper
  def user_status(user)
    curtime = Time.now
    status = ""
    if user.updated_at > curtime - 10.minutes
      status = "active"
    else
      status = "away"
    end
  end
  
  def create_links(message)
    link_matcher = Regexp.new('(http[s]?://[^\s]*)')
    message.gsub(link_matcher, '<a href="\1" target="_blank">\1</a>')
  end
end
