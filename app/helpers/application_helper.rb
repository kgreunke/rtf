# Methods added to this helper will be available to all templates in the application.
module ApplicationHelper
  def user_info_or_login
    if current_user
      render :partial => "layouts/user", :locals => {:user_name => current_user.login}    
    end
  end
end
