require 'spec_helper'

describe ChannelSession do
  before(:each) do
    @valid_attributes = {
      :user_id => 1,
      :channel_id => 1
    }
  end

  it "should create a new instance given valid attributes" do
    ChannelSession.create!(@valid_attributes)
  end
  
  it "should create a new instance if a matching one doesn't exist or update one if it doesn't" do
    ChannelSession.create_or_update(1,1)
    @session = ChannelSession.find(:first, :conditions => {:user_id => 1, :channel_id => 1})
    @session.created_at.should == @session.updated_at
    sleep(1)
    ChannelSession.create_or_update(1,1)
    @new_session = ChannelSession.find(:first, :conditions => {:user_id => 1, :channel_id => 1})
    @new_session.created_at.should < @new_session.updated_at
  end
end
