Factory.define :user do |f|
  f.sequence(:login) {|n| "user#{n}"}
  f.password "password"
  f.password_confirmation { |u| u.password }
  f.sequence(:email) { |n| "email#{n}@freelance-tech.com"}
end

Factory.define :channel do |f|
  f.sequence(:name) { |n| "Channel#{n}"}
  f.private false
end

Factory.define :private_channel, :class => Channel do |f|
  f.private true
  f.sequence(:name) { |n| "private_channel#{n}"}
end

Factory.define :message do |f|
  f.sequence(:message) { |n| "Message number #{n}"}
  f.sequence(:name) { |n| "User#{n}"}
  f.channel_id 1
end

