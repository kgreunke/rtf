unless Date.now
  Date.now = ->
    new Date().getTime()

class window.Message extends Backbone.Model

class window.Messages extends Backbone.Collection
  model: Message

class window.MessageView extends Backbone.View
  tagName: 'div'
  className: 'message'

  initialize: ->
    @template = Handlebars.compile $("#message-template").html()

  render: =>
    message = @model.toJSON()
    message.created_at = @formatDate message.created_at
    message.message = @linkify message.message
    renderedContent = @template message
    ($ @el).html renderedContent
    @

  formatDate: (date) ->
    dateObj = new Date(date)
    if dateObj.getMinutes() < 10 then minutes = "0#{dateObj.getMinutes()}" else minutes = dateObj.getMinutes()
    if dateObj.getHours() < 0 or dateObj.getHours() > 12 then hours = Math.abs dateObj.getHours() - 12 else hours = dateObj.getHours()
    "#{hours}:#{minutes}"

  linkify: (message) ->
    message.replace /(http[s]?:\/\/[^\s]*)/g, (url) ->
      shortname = url
      shortname = url.substring(0,79) + "..." if url.length > 80
      "<a href='#{url}' target='_blank'>#{shortname}</a>"

class window.MessageListView extends Backbone.View
  tagName: 'div'

  initialize: ->
    @collection.bind 'reset', @render
    @collection.bind 'add', @addOne

  events:
    "click": "focus"

  render: =>
    @collection.each (message) =>
      @addOne message
    @

  addOne: (message) =>
    view = new MessageView
      model: message
    $(@el).append view.render().el
    @scroll()

  focus: =>
    @options.inputBox.focus()

  scroll: ->
    $(@el).parent().animate({scrollTop: $(@el).prop('scrollHeight')}, 500)

class window.Channel extends Backbone.Model
  defaults:
    active: false
    class: ''
    unread: 0
    pm: false

  initialize: ->
    # Common options.  Model specific setup goes in setup()
    # Set up the messages collection for this channel
    @messages = new Messages()
    # Set up the view
    @$inputBox = $('<input type="text" placeholder="Type message here"></input>')
    @messageList = new MessageListView
      collection: @messages
      inputBox: @$inputBox
    @setup()

  setup: ->
    @messages.url = @url() + '/messages'
    @messages.fetch()
    # Subscribe to the channel for push updates
    @sub = @collection.transport.subscribe "/channel/public/#{@id}", @getMessage
    @sub.callback (=> console?.log "Subscription to channel #{@get "name"} complete")
    @$inputBox.keydown @sendMessage

  active: ->
    @get 'active'

  getMessage: (msg) =>
    @messages.add msg
    unless @active()
      @set
        unread: @attributes.unread + 1
      $.publish "channel:unread", [1, @attributes.pm]

  sendMessage: (event) =>
    if event.keyCode == 13
      message =
        message:
          name: @collection.currentUser
          message: @$inputBox.val()
          channel_id: @attributes.id
        authenticity_token: @collection.token
      $.ajax
        url: '/messages.json'
        type: 'POST'
        contentType: 'application/json'
        processData: false
        dataType: 'json'
        data: JSON.stringify(message)
        success: @clearInput 

  clearInput: =>
    @$inputBox.val ""

  renderMessages: =>
    $('#input').children().detach()
    $('#input').append @$inputBox
    @$inputBox.focus()
    $('#messages').children().detach()
    $('#messages').append @messageList.el
    $(window).trigger 'resize'
    @messageList.scroll()
    $.publish "channel:unread", [-@attributes.unread, @attributes.pm]
    @attributes.unread = 0
    $.publish "channel:status", [@cid]

class window.Channels extends Backbone.Collection
  model: Channel
  url: '/channels'

class window.ChannelView extends Backbone.View
  tagName: 'li'

  events:
    "click a": "getMessages"

  initialize: ->
    @template = Handlebars.compile $("#user-template").html()
    @model.bind 'change', @render

  render: =>
    console?.log "Rendering view for #{@model.get 'name'}"
    $(@el).attr('class', @model.get 'class').html @template @model.toJSON()
    @

  getMessages: ->
    if event.preventDefault
      event.preventDefault()
    else
      event.returnValue = false
    @model.renderMessages() unless @model.active()

class window.ChannelListView extends Backbone.View
  tagName: 'nav'
  className: 'main-nav'
  viewClass: ChannelView
  title: "Channels"

  initialize: ->
    @template = Handlebars.compile $("#user-list-template").html()
    @collection.bind 'add', @addOne
    @collection.bind 'reset', @render

  render: =>
    $(@el).empty()
    $(@el).html @template
      title: @title
    @collection.each (model) =>
      @addOne model
    @

  addOne: (model) =>
    view = new @viewClass({model: model})
    @$('ul').append view.render().el

class window.User extends Channel
  defaults:
    active: false
    class: ''
    unread: 0
    avatar_url: "/assets/avatars/default.png"
    pm: true

  setup: ->
    $.subscribe 'user:status', @setStatus
    $.subscribe 'user:message', @getMessage
    $(@$inputBox).keydown @sendMessage
    @channel = "/channel/priv/" + [@collection.currentUser, @attributes.name].sort().join ""
    @sub = @collection.transport.subscribe @channel, @getMessage
    @set {avatar_url: "https://robohash.org/#{@get('name').toLowerCase()}?size=48x48&bgset=bg2"}

  valid: =>
    @attributes.name?

  setStatus: (msg) =>
    if msg.name == @attributes.name then @set {status: msg.status}

  sendMessage: (event) =>
    if event.keyCode == 13
      message =
        name: @collection.currentUser
        to: @attributes.name
        created_at: Date.now()
        message: @$inputBox.val()
      @collection.transport.publish @channel, message
      @$inputBox.val ""

class window.Users extends Channels
  model: User

  addUnique: (user) ->
    @add user unless @detect( (u) -> user.name == u.get('name') )

class window.UserView extends ChannelView

class window.UserListView extends ChannelListView
  viewClass: UserView
  title: "Users"

class window.CurrentUser extends User
  setup: ->
    @set {avatar_url: "https://robohash.org/#{@get('name').toLowerCase()}?size=48x48&bgset=bg2"}

class window.CurrentUserView extends Backbone.View
  tagName: 'section'
  id: 'user-info'

  initialize: ->
    @template = Handlebars.compile $("#current-user-template").html()

  render: =>
    $(@el).html @template @model.toJSON()
    @


class window.AppView extends Backbone.View
  el: "#pagewrapper"

  initialize: ->
    @template = Handlebars.compile $("#app-template").html()
    @currentUserView = @options.currentUserView
    @userListView = @options.userListView
    @channelListView = @options.channelListView


  render: =>
    $(@el).empty().append @template {}
    $("#usercolumn").append @currentUserView.render().el
    $("#usercolumn").append @channelListView.render().el
    $("#usercolumn").append @userListView.render().el
    @

class window.Title extends Backbone.Model
  defaults:
    cm: 0
    pm: 0

  initialize: ->
    @.bind 'change', @unreads

  unreads: ->
    if @attributes["pm"] + @attributes["cm"] > 0 then @set {total: true} else @set {total: false}

  addPM: (num) =>
    @set {pm: @attributes["pm"] + num}

  addCM: (num) =>
    @set {cm: @attributes["cm"] + num}

class window.TitleView extends Backbone.View

  initialize: ->
    @template = Handlebars.compile "{{#if total}}{{cm}}|{{pm}} - {{/if}}rtf"
    @model.bind 'change', @render

  render: =>
    document.title = @template @model.toJSON()
    #$(@el).empty().append @template @model.toJSON()
    @

class window.RTF
  constructor: (options={})->
    @currentUser = new CurrentUser options.currentUser
    #Faye.Logging.logLevel = 'debug'
    #Faye.logger = (msg) -> console.log msg
    #@faye = new Faye.Client 'http://localhost:9293/framework/push', {timeout: 60}
    @faye = new Faye.Client window.location.href + 'framework/push', {timeout: 120}
    @faye.disable 'websocket'
    @users = new Users()
    @users.transport = @faye
    @users.currentUser = @currentUser.get 'name'
    @channels = new Channels()
    @channels.transport = @faye
    @channels.currentUser = @currentUser.get 'name'
    @channels.token = options.token
    @title = new Title()
    @titleView = new TitleView {model: @title}
    @appView = new AppView
      currentUserView:  new CurrentUserView {model: @currentUser}
      userListView:     new UserListView {collection: @users }
      channelListView:  new ChannelListView {collection: @channels}

  init: ->
    @localStatus = "active"
    @appView.render()
    @titleView.render()
    @resizeMessages()
    @events()
    @subs()
    @channels.fetch()

  events: ->
    $(window).blur @manageStatusEvent
    $(window).focus @manageStatusEvent
    $(window).resize @resizeMessages

  subs: ->
    @controlSub = @faye.subscribe "/control", @control
    @controlSub.callback (=> @send "refresh")
    #@userPrivateSub = @faye.subscribe "/channel/priv/#{@currentUser.get 'name'}", @privmsg
    $.subscribe "channel:status", @updateChannelStatus
    $.subscribe "channel:unread", @updateUnread

  send: (action, payload={}, channel="/control") =>
    message =
      action: action
      payload: payload
    @faye.publish channel, message
    
  control: (message) =>
    console?.log "Recieved control message: #{JSON.stringify message}"
    switch message.action
      when "status" then @manageStatusEvent message.payload
      when "update" then @users.addUnique message.payload unless message.payload.name == @currentUser.get('name')
      when "refresh" then @send "update", {name: @currentUser.get('name'), status: @currentUser.get('status')}

  privmsg: (message) =>
    console?.log "Recieved privage message: #{JSON.stringify message}"
    $.publish "user:message", [message]

  manageStatusEvent: (status={}) =>
    switch status.type
      when "blur" then @setCurrentStatus "away"
      when "focus" then @setCurrentStatus "active"
      else
        console?.log "Updating user status: #{JSON.stringify status}"
        $.publish "user:status", [status]

  setCurrentStatus: (status) ->
    clearTimeout @statusTimeout
    @localStatus = status
    if @localStatus != @currentUser.get('status')
      @statusTimeout = setTimeout (=>
        @currentUser.set {status}
        @sendCurrentStatus()
      ), 5000
      console?.log "Queued remote status change to: #{status}"

  sendCurrentStatus: =>
    currentstatus =
      name: @currentUser.get 'name'
      status: @currentUser.get 'status'
    @send "status", currentstatus

  getCurrentStatus: =>
    @currentUser.get 'status'

  resizeMessages: ->
    $el = $('#messages')
    heightOffset = $el.outerHeight() - $el.height()
    $el.css 'height', window.innerHeight - $('#input').outerHeight() - heightOffset

  updateChannelStatus: (id) =>
    @channels.each (channel) =>
      @toggleChannelStatus channel, id
    @users.each (user) =>
      @toggleChannelStatus user, id

  updateUnread: (num, priv) =>
    if priv then @title.addPM num else @title.addCM num

  toggleChannelStatus: (channel, id) ->
    if channel.get 'active'
      channel.set
        active: false
        class: ''
    else if channel.cid == id
      channel.className = "active"
      channel.set
        active: true
        class: 'current'
