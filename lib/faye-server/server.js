var http = require('http')
  , faye = require('faye')
  ;

var bayeux = new faye.NodeAdapter({mount: '/framework/push', timeout: 115});
//var sub = bayeux.getClient().subscribe('/**', function(message){
//  console.log("DEBUG: " + JSON.stringify(message));
//  });
var server = http.createServer(function(req,res){
  var postData = "";
  req.addListener("data", function(chunk){
    postData += chunk;
    });

  req.addListener("end", function() {
    console.log("Got: " + postData);
    if (req.headers.origin || req.method == "GET") {
      console.log("Not local, dumping.");
      res.writeHead(404);
    } else {
      if (postData.length > 0) {
        var post = JSON.parse(postData);
        bayeux.getClient().publish(post.channel, post.data);
      } else {
        console.log("  Empty request. Debug output:");
        console.log("    " + req.method + ": " + req.url);
        console.log("    " + JSON.stringify(req.headers));
      }
      res.writeHead(200);
    }
    res.end('Done');
  });
});

bayeux.attach(server);
server.listen(10000, function(){
  console.log("Faye server started on 10000");
});
