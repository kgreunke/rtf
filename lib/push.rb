require 'uri'
  class Push
    include HTTParty
    base_uri 'http://localhost:10000'

    def self.message(channel, data)
      message = {:channel => channel, :data => data}.to_json
      #post('/framework/push',  :body => URI.escape(message, Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")))
      post('/',  :body => message)
    end
  end
