class EnlargeMessageField < ActiveRecord::Migration
  def self.up
    change_column :messages, :message, :string, :limit => 2048
  end

  def self.down
  end
end
