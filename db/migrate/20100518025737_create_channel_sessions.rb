class CreateChannelSessions < ActiveRecord::Migration
  def self.up
    create_table :channel_sessions do |t|
      t.integer :user_id,     :null => false
      t.integer :channel_id,  :null => false

      t.timestamps
    end
  end

  def self.down
    drop_table :channel_sessions
  end
end
