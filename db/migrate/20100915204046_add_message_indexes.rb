class AddMessageIndexes < ActiveRecord::Migration
  def self.up
    add_index(:messages, :id)
    add_index(:messages, :channel_id)
  end

  def self.down
    remove_index(:messages, :id)
    remove_index(:messages, :channel_id)
  end
end
